# CI-CD
## Summary 

This repository contains common CICD templates for LTSHealth.

## Available Templates

- `ci-cd/templates/jobs/frontend/react-native/.gitlab-ci.yml`: This template is used by both `kiosk-ui` and `consumer-ui` frontend repositories. It consists of common stages along with their associated jobs. Here are the stages:
    - validation: runs a linter and formatter to clean up the code
    - test: runs jest unit tests and kicks off a sast job
    - build: statically bundles the web portion of the application
    - publish: currently does nothing
    - deploy: deploys the statically bundled artifact to AWS Amplify
